==========================
Tebe 0.0.0 (alpha version)
==========================

Tebe sphinx writer.

Requirements:

1. Python 2.7
#. pyqt4
#. sphinx
#. rst2pdf
#. rst2html
#. decutils
#. recommonmark

If you want start the program without installing requirements, you need:

- install docker engine and docker-compose;
- execute command::

   xhost +local:docker  

- go to docker directory;
- execute command::

   docker-compose up --build

Now will be build container with tebe and run programm. You can open 
folder with examples (/srv/examples).

