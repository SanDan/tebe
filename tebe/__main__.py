'''
--------------------------------------------------------------------------
Copyright (C) 2017 Lukasz Laba <lukaszlab@o2.pl>

This file is part of Tebe.

Tebe is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tebe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------
'''

import sys
import os
import subprocess

from PyQt4 import QtGui, QtCore

from gui.Editor import Editor
from gui.Preview import Preview
from gui.Tree import Tree

from pycore.Core import Content, SphinxBuilder, Rst2PdfBuilder
from pycore.markup_utils import rst_to_html
from pycore.core_utils import abspath, APP_PATH

from info import appdata

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent=None):
        super(MainWindow, self).__init__(parent)
        #---
        self.Tree = Tree()
        self.Editor = Editor()
        #---
        self.tab_widget = QtGui.QTabWidget()
        self.Preview_live = Preview()
        self.Preview_this = Preview()
        self.Preview_all = Preview()
        self.tab_widget.addTab(self.Preview_live, "this page live")
        self.tab_widget.addTab(self.Preview_this, "sphinx build this page")
        self.tab_widget.addTab(self.Preview_all, "sphinx build index page")
        #---
        splitter = QtGui.QSplitter(QtCore.Qt.Horizontal)
        self.setCentralWidget(splitter)
        splitter.addWidget(self.Tree)
        splitter.addWidget(self.Editor)
        splitter.addWidget(self.tab_widget)
        #---
        self.setupActions()
        self.connectSignals()
        #---
        self.createMenus()
        self.createToolBars()
        self.showMaximized()
        #---
        self.set_apptitle()
        self.status = self.statusBar()
        self.setWindowIcon(QtGui.QIcon("icons/logo.png"))
        
    def setupActions(self):
        self.openAction = QtGui.QAction(QtGui.QIcon(abspath("icons/open_file.png")),"Open file", self)
        self.openAction.setShortcut("Ctrl+O")
        self.openAction.setStatusTip("Open File")
        self.openAction.triggered.connect(self.openFile)

        self.openFolderAction = QtGui.QAction(QtGui.QIcon(abspath("icons/open_folder.png")), "Open Folder", self)
        self.openFolderAction.setShortcut("Ctrl+Shift+O")
        self.openFolderAction.setStatusTip("Open Folder")
        self.openFolderAction.triggered.connect(self.openFolder)        

        self.saveAction = QtGui.QAction(QtGui.QIcon(abspath("icons/save.png")), "Save File", self)
        self.saveAction.setShortcut("Ctrl+S")
        self.saveAction.setStatusTip("Save File")
        self.saveAction.triggered.connect(self.saveFile)  
        
        self.saveAsAction = QtGui.QAction(QtGui.QIcon(abspath("icons/save_as.png")), "Save As File", self)
        self.saveAsAction.setShortcut("Ctrl+Shift+S")
        self.saveAsAction.setStatusTip("Save File As")
        self.saveAsAction.triggered.connect(self.saveFileAs) 

        self.quitAction = QtGui.QAction(QtGui.QIcon(abspath("icons/quit.png")), "Quit", self)
        self.quitAction.setShortcut("Ctrl+QCtrl+Q")
        self.quitAction.setStatusTip("Quit")
        self.quitAction.triggered.connect(self.close) 
 
        self.buildHTMLAction = QtGui.QAction(QtGui.QIcon(abspath("icons/build.png")), "Sphinx build", self)
        self.buildHTMLAction.setShortcut("Ctrl+B")
        self.buildHTMLAction.setStatusTip("Build HTML")
        self.buildHTMLAction.triggered.connect(self.buildHTML) 

        self.sphinx_buildPDFAction = QtGui.QAction(QtGui.QIcon(abspath("icons/pdf_sphinx.png")), "Export to PDF", self)
        self.sphinx_buildPDFAction.setShortcut("Ctrl+Shift+B")
        self.sphinx_buildPDFAction.setStatusTip("Build PDF")
        self.sphinx_buildPDFAction.triggered.connect(self.buildPDF) 

        self.this_buildPDFAction = QtGui.QAction(QtGui.QIcon(abspath("icons/pdf_this.png")), "Export to PDF", self)
        self.this_buildPDFAction.setShortcut("Ctrl+Shift+B")
        self.this_buildPDFAction.setStatusTip("Build PDF")
        self.this_buildPDFAction.triggered.connect(self.build_this_PDF) 

        self.printAction = QtGui.QAction(QtGui.QIcon(abspath("icons/print.png")), "Print live preview", self)
        self.printAction.setShortcut("Ctrl+Shift+P")
        self.printAction.setStatusTip("Print live preview")
        self.printAction.triggered.connect(self.directPrint)

        self.aboutAction = QtGui.QAction(QtGui.QIcon(abspath("icons/about.png")), "Info", self)
        self.aboutAction.setShortcut("Ctrl+Shift+P")
        self.aboutAction.setStatusTip("App info")
        self.aboutAction.triggered.connect(self.about)

    def connectSignals(self):
        QtCore.QObject.connect(self.Editor, QtCore.SIGNAL("textChanged()"),self.live_update)

    def createMenus(self):
        self.fileMenu = self.menuBar().addMenu("File")
        self.fileMenu.addAction(self.openAction)
        self.fileMenu.addAction(self.openFolderAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.saveAction)
        self.fileMenu.addAction(self.saveAsAction)
        self.fileMenu.addSeparator()
        self.fileMenu.addAction(self.quitAction)
        #---
        self.buildMenu = self.menuBar().addMenu("Sphinx build")
        self.buildMenu.addAction(self.buildHTMLAction)
        self.buildMenu.addAction(self.sphinx_buildPDFAction)
        #---
        self.this_buildMenu = self.menuBar().addMenu("This page build")
        self.this_buildMenu.addAction(self.this_buildPDFAction)
        self.this_buildMenu.addAction(self.printAction)
        #---
        self.this_buildMenu = self.menuBar().addMenu("Help")
        self.this_buildMenu.addAction(self.aboutAction)
        
    def createToolBars(self):
        self.fileToolBar = self.addToolBar("File")
        self.fileToolBar.addAction(self.openAction)
        self.fileToolBar.addAction(self.openFolderAction)
        self.fileToolBar.addAction(self.saveAction)
        #--
        self.sphinxbuildToolBar = self.addToolBar("Sphinx build")
        self.sphinxbuildToolBar.addAction(self.buildHTMLAction)
        self.sphinxbuildToolBar.addAction(self.sphinx_buildPDFAction)
        #--
        self.thisbuildToolBar = self.addToolBar("This build")
        self.thisbuildToolBar.addAction(self.printAction)
        self.thisbuildToolBar.addAction(self.this_buildPDFAction)

    #---------------------------------
        
    def about(self):
        QtGui.QMessageBox.information(None, 'Info', appdata._about)
        
    #---------------------------------
    
    def openFile(self, file_path=None):
        if not file_path:
            file_path = QtGui.QFileDialog.getOpenFileName(  caption = 'Open markup file',
                                                            directory = APP_PATH,
                                                            filter = "Markup File (*.rst *.md)")
        #---
        if file_path:    
            file_path = str(file_path)
            #---
            selcted_filename = os.path.basename(file_path)
            selcted_dirname = os.path.dirname(file_path)
            #---
            self.handleFileChanged(selcted_dirname, selcted_filename)

    def openFolder(self):
        dir_path = QtGui.QFileDialog.getExistingDirectory(  self,
                                                            directory = APP_PATH,
                                                            caption = 'Open folder')
        #---
        if dir_path:
            dir_path = str(dir_path)
            self.handleFileChanged(dir_path)

    def saveFile(self):
        if self.Editor.file_path:
            text = self.Editor.toPlainText()
            text = str(text)
            try:
                f = open(self.Editor.file_path, "wb")
                f.write(text)
                f.close()
                QtGui.QMessageBox.information(self, 'Info', 'Saved to %s'% self.Editor.file_path)
            except IOError:
                QtGui.QMessageBox.information( self, 'Info', "Unable to save to %s" % self.Editor.file_path)
            self.live_update()
        else:
            self.saveFileAs()

    def saveFileAs(self):
        file_path = QtGui.QFileDialog.getSaveFileName(  caption = 'Save markup file as',
                                                        directory = APP_PATH,
                                                        filter = "Markup File (*.rst *.md)")
        #---
        if file_path:
            file_path = str(file_path)
            text = self.Editor.toPlainText()
            text = str(text)
            try:
                f = open(file_path, "wb")
                f.write(text)
                f.close()
                QtGui.QMessageBox.information(self, 'Info', 'Saved to %s'% self.Editor.file_path)
            except IOError:
                QtGui.QMessageBox.information( self, 'Info', "Unable to save to %s" % self.Editor.file_path)
            self.openFile(file_path)
    
    #---------------------------------

    def handleFileChanged(self, dir, filename=None):
        if not filename:
            filename = "index.rst"
        #---
        Content.set_source_dir(dir)
        #---
        self.Editor.open_file(os.path.join(dir, filename))
        #---
        self.Tree.load_from_dir(dir)
        #---
        self.live_update()
        #---
        self.reload_sphinx_previews()
        #---
        self.set_apptitle()
    
    def reload_sphinx_previews(self):
        index_html_path = os.path.join(SphinxBuilder.tmp_html_dir, 'index.html')
        self.Preview_all.show_html(index_html_path)
        #---
        this_file_html = os.path.basename(self.Editor.file_path)
        this_file_html = this_file_html.replace('.rst', '.html')
        this_file_html = this_file_html.replace('.md', '.html')
        this_file_html_path = os.path.join(SphinxBuilder.tmp_html_dir, this_file_html)
        self.Preview_this.show_html(this_file_html_path)
    
    #---------------------------------
        
    def buildHTML(self):
        self._cursor_wait(True)
        SphinxBuilder.build_html()
        self._cursor_wait(False)
        #---
        self.reload_sphinx_previews()

    def buildPDF(self):
        self._cursor_wait(True)
        SphinxBuilder.build_pdf()
        self._cursor_wait(False)
        #---
        QtGui.QMessageBox.information(None, 'Info', 'Pdf saved as %s'% SphinxBuilder.where_pdf_saved)
        
    def build_this_PDF(self):
        if self.Editor.is_rst_file():
            self._cursor_wait(True)
            Rst2PdfBuilder.build_pdf_from_rst_file(self.Editor.file_path)
            self._cursor_wait(False)
        #---
        QtGui.QMessageBox.information(None, 'Info', 'Pdf saved as %s'% Rst2PdfBuilder.where_pdf_saved)
  
    #---------------------------------

    def directPrint(self):
        dialog = QtGui.QPrintDialog()
        if dialog.exec_() == QtGui.QDialog.Accepted:
            self.Preview_live.print_(dialog.printer())

    #---------------------------------
            
    def live_update(self):
        markup_text = self.Editor.toPlainText()
        markup_text = u'%s'%markup_text
        html = rst_to_html(markup_text)
        #---
        self.Preview_live.setHtml(html)  
    
    #---------------------------------

    def _cursor_wait(self, wait=False):
        if wait:
            QtGui.QApplication.setOverrideCursor(QtGui.QCursor(QtCore.Qt.WaitCursor))
        else:
            QtGui.QApplication.restoreOverrideCursor()
            
    def set_apptitle(self):
        apptitle = appdata._appname + ' ' + appdata._version + ' - sphinx writer '
        if self.Editor.file_path:
            apptitle += ' [ ' + os.path.basename(self.Editor.file_path) + ' ]'
        self.setWindowTitle(apptitle)
    
    #---------------------------------
    
    def closeEvent(self, event):
        SphinxBuilder.close()
        event.accept()
        
def main():
    global app, window
    app = QtGui.QApplication(sys.argv)
    window = MainWindow()
    #---
    window.Preview_live.setHtml('<h3><< Try to write something</h3>')
    #---
    sys.exit(app.exec_())

if __name__ == '__main__':
    main()