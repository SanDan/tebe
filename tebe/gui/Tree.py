'''
--------------------------------------------------------------------------
Copyright (C) 2017 Lukasz Laba <lukaszlab@o2.pl>

This file is part of Tebe.

Tebe is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

Tebe is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
--------------------------------------------------------------------------
'''

from PyQt4 import QtGui, QtCore

class Tree(QtGui.QTreeView):
	def __init__(self, parent=None):
		super(Tree, self).__init__(parent)
		self.dir_path = None
		#---
		self.setMaximumWidth(180)

	def load_from_dir(self, dir_path):
		#---if it's the same dir as before, return to avoid redrawing
		if dir_path == self.dir_path:
			return None
		#---Store the path info
		self.dir_path = dir_path
		#---Link the tree to a model
		model = QtGui.QFileSystemModel()
		model.setRootPath(dir_path)
		self.setModel(model)
		#---Set the tree's index to the root of the model
		indexRoot = model.index(model.rootPath())
		self.setRootIndex(indexRoot)
		#---Display tree cleanly
		self.hide_unwanted_info()
		
	def hide_unwanted_info(self):
		#---Hide tree size and date columns
		self.hideColumn(1)
		self.hideColumn(2)
		self.hideColumn(3)
		#---Hide tree header
		self.setHeaderHidden(True)

	def selectionChanged(self, selected, deselected):
		super(Tree, self).selectionChanged(selected, deselected)
		indexes = selected.indexes()
		if indexes:
			#---Handle fileChanged event in main window
			new_filename = self.model().data(indexes[0])
			new_filename = str(new_filename.toPyObject())
			main_win = self.parent().parent()
			main_win.handleFileChanged(self.dir_path, new_filename)